package com.example.marveldemoapp.base.decorator.request

abstract class Request {
    abstract fun getParams(): MutableMap<String, String?>
}