package com.example.marveldemoapp.base.decorator.viewHolder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.marveldemoapp.base.interfaces.Model

abstract class ViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) where T : Model {
    abstract fun bind(model: T?, onClickedItem: (view: View, T?) -> Unit)
    abstract fun unBind()
    abstract fun loadImage(model: T?)
}