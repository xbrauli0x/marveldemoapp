package com.example.marveldemoapp.base.decorator.viewHolder

import android.util.Log
import android.view.View
import com.example.marveldemoapp.base.interfaces.Model
import com.example.marveldemoapp.models.Thumbnail
import com.example.marveldemoapp.utils.ImageLoader

class BaseViewHolder<T>(itemView: View, private var handler: ImageLoader.Handler) :
    ViewHolder<T>(itemView) where T : Model {

    override fun bind(model: T?, onClickedItem: (view: View, T?) -> Unit) {
        itemView.setOnClickListener { onClickedItem(it, model) }
        loadImage(model)
    }

    override fun unBind() {
        itemView.setOnClickListener(null)
    }

    override fun loadImage(model: T?) {
        val thumbnail: Thumbnail? = model?.getComicThumbnail()
        if (thumbnail != null) {
            val url: String? = thumbnail.url
            Log.d("Img-Comic", "${model.getComicTitle()} : $url")
            ImageLoader.loadImage(handler, url)
        }
    }
}