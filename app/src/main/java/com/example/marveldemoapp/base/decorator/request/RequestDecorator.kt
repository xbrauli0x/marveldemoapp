package com.example.marveldemoapp.base.decorator.request

abstract class RequestDecorator : Request() {

    private val request = BaseRequest()

    override fun getParams(): MutableMap<String, String?> {
        return request.getParams()
    }
}