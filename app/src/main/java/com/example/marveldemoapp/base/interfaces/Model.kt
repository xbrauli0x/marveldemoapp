package com.example.marveldemoapp.base.interfaces

import com.example.marveldemoapp.models.Thumbnail

interface Model {
    fun getComicTitle(): String?
    fun getComicThumbnail(): Thumbnail?
}