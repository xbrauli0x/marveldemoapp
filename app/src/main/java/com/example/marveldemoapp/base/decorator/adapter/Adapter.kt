package com.example.marveldemoapp.base.decorator.adapter

import androidx.recyclerview.widget.RecyclerView
import com.example.marveldemoapp.base.decorator.viewHolder.ViewHolder
import com.example.marveldemoapp.base.interfaces.Model

abstract class Adapter<T, VH> : RecyclerView.Adapter<VH>() where VH : ViewHolder<T>, T : Model {
    abstract fun getItem(index: Int): T?
    abstract fun updateData(fromIndex: Int, data: List<T>?)
}