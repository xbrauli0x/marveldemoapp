package com.example.marveldemoapp.base.decorator.adapter

import android.view.View
import android.view.ViewGroup
import com.example.marveldemoapp.base.decorator.viewHolder.ViewHolderDecorator
import com.example.marveldemoapp.base.interfaces.Model

abstract class AdapterDecorator<T, VH>(onClickListener: (view: View, T?) -> Unit) :
    Adapter<T, VH>() where VH : ViewHolderDecorator<T>, T : Model {

    private val adapter: BaseAdapter<T, VH> = BaseAdapter(onClickListener)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return adapter.onCreateViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        adapter.onBindViewHolder(holder, position)
    }

    override fun getItemCount(): Int {
        return adapter.itemCount
    }

    override fun onViewDetachedFromWindow(holder: VH) {
        adapter.onViewDetachedFromWindow(holder)
    }

    override fun getItem(index: Int): T? {
        return adapter.getItem(index)
    }

    override fun updateData(fromIndex: Int, data: List<T>?) {
        adapter.updateData(fromIndex, data)
        notifyDataSetChanged()
    }
}