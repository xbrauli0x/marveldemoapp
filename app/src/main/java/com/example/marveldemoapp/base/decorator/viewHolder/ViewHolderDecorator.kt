package com.example.marveldemoapp.base.decorator.viewHolder

import android.view.View
import com.example.marveldemoapp.base.interfaces.Model
import com.example.marveldemoapp.utils.ImageLoader

@Suppress("LeakingThis")
abstract class ViewHolderDecorator<T>(itemView: View) :
    ViewHolder<T>(itemView), ImageLoader.Handler where T : Model {

    private val viewHolder: ViewHolder<T> = BaseViewHolder(itemView, this)

    override fun bind(model: T?, onClickedItem: (view: View, T?) -> Unit) {
        viewHolder.bind(model, onClickedItem)
    }

    override fun unBind() {
        viewHolder.unBind()
    }

    override fun loadImage(model: T?) {
        viewHolder.loadImage(model)
    }

    override fun getItemView(): View {
        return itemView
    }
}