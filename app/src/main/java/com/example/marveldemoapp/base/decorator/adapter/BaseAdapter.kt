package com.example.marveldemoapp.base.decorator.adapter

import android.view.View
import android.view.ViewGroup
import com.example.marveldemoapp.base.decorator.viewHolder.ViewHolder
import com.example.marveldemoapp.base.interfaces.Model


class BaseAdapter<T, VH>(private var onClickListener: (view: View, T?) -> Unit) :
    Adapter<T, VH>() where VH : ViewHolder<T>, T : Model {

    private var data: MutableList<T>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return super.createViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val model = getItem(position)
        holder.bind(model, onClickListener)
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    override fun getItem(index: Int): T? {
        return data?.get(index)
    }

    override fun updateData(fromIndex: Int, data: List<T>?) {
        when {
            dataSetIsEmpty() -> {
                setAsCurrentDataSet(data)
            }
            elementFromIndexExists(fromIndex) -> {
                updateWithNewData(fromIndex, data)
            }
            else -> {
                addToCurrentDataSet(fromIndex, data)
            }
        }
    }

    private fun dataSetIsEmpty(): Boolean {
        return this.data?.isEmpty() ?: true
    }

    private fun setAsCurrentDataSet(data: List<T>?) {
        this.data = data as MutableList<T>
    }

    private fun elementFromIndexExists(index: Int): Boolean {
        return data?.size ?: 0 > index
    }

    private fun updateWithNewData(fromIndex: Int, data: List<T>?) {
        deleteOldItemsFromDataSet(fromIndex, data)
        addToCurrentDataSet(fromIndex, data)
    }

    private fun deleteOldItemsFromDataSet(fromIndex: Int, data: List<T>?) {
        val count = data?.size!!
        for(i in fromIndex until count) {
            this.data?.removeAt(fromIndex)
        }
    }

    private fun addToCurrentDataSet(fromIndex: Int, data: List<T>?) {
        this.data?.addAll(fromIndex, data!!)
    }
}