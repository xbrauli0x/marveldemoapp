package com.example.marveldemoapp.base.decorator.request

import com.example.marveldemoapp.utils.toMD5


class BaseRequest : Request() {

    private var apiKey: String? = null
    private var hash: String? = null
    private var privateKey: String? = null
    private var timestamp: Long? = null

    init {
        apiKey = "13ef0e1a3cda71f28e1c7278b5d1abd2"
        privateKey = "bcab2ae8ff7d4b24f24ba0750caf3c3139b78c77"
    }

    override fun getParams(): MutableMap<String, String?> {
        generateHash()
        return createAndReturnQueryMap()
    }

    private fun generateHash() {
        timestamp = System.currentTimeMillis()
        val hashCandidate: String = timestamp.toString() + privateKey + apiKey
        hash = hashCandidate.toMD5()
    }

    private fun createAndReturnQueryMap(): MutableMap<String, String?> {
        val params = HashMap<String, String?>()

        params["ts"] = timestamp.toString()
        params["apikey"] = apiKey
        params["hash"] = hash

        return params
    }
}