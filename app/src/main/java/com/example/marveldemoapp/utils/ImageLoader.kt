package com.example.marveldemoapp.utils

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.marveldemoapp.R

class ImageLoader {

    // Not really a fan of this interface naming, but for the moment couldn't think on something else
    interface Handler {
        fun getItemView(): View
        fun getImageViewTarget(): ImageView
    }

    companion object {
        fun loadImage(handler: Handler, url: String?) {
            Glide.with(handler.getItemView())
                .load(url)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.ic_launcher_foreground)
                .error(R.drawable.ic_menu_camera)
                .into(handler.getImageViewTarget())
        }
    }
}