package com.example.marveldemoapp.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.example.marveldemoapp.R
import org.koin.core.KoinComponent
import org.koin.core.inject

class Utils {
    companion object : KoinComponent {
        private val context: Context by inject()

        fun hasNetwork(): Boolean? {
            var isConnected: Boolean? = false
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
            if (activeNetwork != null && activeNetwork.isConnected)
                isConnected = true
            return isConnected
        }

        fun getMarvelApiUri(path: String?, extension: String?): String? {
            val screenQualifier: String? = getAPIScreenQualifier()
            return "$path/$screenQualifier.$extension"
        }

        private fun getAPIScreenQualifier(): String? {
            return context.getString(R.string.screen_qualifier)
        }
    }
}