package com.example.marveldemoapp.home_detail

import android.view.View
import android.widget.ImageView
import com.example.marveldemoapp.base.decorator.viewHolder.ViewHolderDecorator
import com.example.marveldemoapp.models.MarvelItem
import kotlinx.android.synthetic.main.item_marvel_character.view.*

class HomeDetailViewHolder(itemView: View) : ViewHolderDecorator<MarvelItem>(itemView) {

    override fun bind(model: MarvelItem?, onClickedItem: (view: View, MarvelItem?) -> Unit) {
        super.bind(model, onClickedItem)
        itemView.txtName.text = model?.name
    }

    override fun getImageViewTarget(): ImageView {
        return itemView.ivMarvelThumbnail
    }
}