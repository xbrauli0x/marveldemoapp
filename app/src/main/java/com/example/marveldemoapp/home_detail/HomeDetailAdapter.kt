package com.example.marveldemoapp.home_detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.marveldemoapp.R
import com.example.marveldemoapp.base.decorator.adapter.AdapterDecorator
import com.example.marveldemoapp.base.decorator.viewHolder.ViewHolderDecorator
import com.example.marveldemoapp.models.MarvelItem

class HomeDetailAdapter(onClickListener: (view: View, MarvelItem?) -> Unit) :
    AdapterDecorator<MarvelItem, ViewHolderDecorator<MarvelItem>>(onClickListener) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolderDecorator<MarvelItem> {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_marvel_character, parent, false)
        return HomeDetailViewHolder(v)
    }

}