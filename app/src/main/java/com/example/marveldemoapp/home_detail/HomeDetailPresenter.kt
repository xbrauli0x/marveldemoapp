package com.example.marveldemoapp.home_detail

import com.example.marveldemoapp.base.BasePresenter
import com.example.marveldemoapp.models.MarvelItem

class HomeDetailPresenter(var view: HomeDetailContract.View?) : BasePresenter(view),
    HomeDetailContract.Presenter, HomeDetailContract.Interactor.ResponseListener {

    private val interactor: HomeDetailContract.Interactor by lazy { HomeDetailInteractor(this) }

    override fun getMarvelCharacters(offset: Int?) {
        view?.showLoading()
        interactor.fetchMarvelData(offset)
    }

    override fun onMarvelDataFetched(fromIndex: Int, data: List<MarvelItem>?) {
        view?.stopLoading()
        view?.renderMarvelItems(fromIndex, data)
    }

    override fun onErrorFetchingData(msg: String?) {
        view?.stopLoading()
        view?.onErrorWhenFetching(msg)
    }
}