package com.example.marveldemoapp.home_detail

import android.util.Log
import com.example.marveldemoapp.models.request.MarvelCharactersRequest
import com.example.marveldemoapp.network.BaseClient
import com.example.marveldemoapp.utils.MarvelResponseHandler

class HomeDetailInteractor(var responseListener: HomeDetailContract.Interactor.ResponseListener?) :
    HomeDetailContract.Interactor {

    override fun fetchMarvelData(offset: Int?) {
        val request = MarvelCharactersRequest()
        request.offset = offset
        val params = request.getParams()
        Log.d("HomeDetailInteractor", "MarvelCharactersRequest params : $params")
        val call = BaseClient.provideMarvelApi().getCharacters(params)
        val responseHandler = MarvelResponseHandler(call,
            {
                responseListener?.onMarvelDataFetched(request.offset!!, it)
            },
            {
                responseListener?.onErrorFetchingData(it)
            })
        responseHandler.makeRequest()
    }
}