package com.example.marveldemoapp.home_detail

import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.marveldemoapp.R
import com.example.marveldemoapp.base.BaseActivity
import com.example.marveldemoapp.base.BaseContract
import com.example.marveldemoapp.detail_screen.DetailScreenActivity
import com.example.marveldemoapp.models.MarvelItem
import com.example.marveldemoapp.utils.EndlessRecyclerViewScrollListener
import com.example.marveldemoapp.widgets.GridItemDecoration
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_home_detail.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.item_marvel_character.view.*

class HomeDetailActivity : BaseActivity(), HomeDetailContract.View {

    private var currentOffset: Int? = null
    private val presenter: HomeDetailContract.Presenter by lazy { HomeDetailPresenter(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (deviceNeedsToBeOnPortraitMode()) {
            forceToPortraitMode()
        }
        setContentView(R.layout.activity_main)
        setUpToolbar()
        val dis: Int = resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK
        Log.d("DIS Screen", "" + dis)
        setUpMarvelCharactersRecyclerView()
        setUpSwipeRefreshLayout()
        fetchInitialMarvelCharacters()
    }

    override fun showLoading() {
        swipeRefreshLayout.isRefreshing = true
    }

    override fun stopLoading() {
        swipeRefreshLayout.isRefreshing = false
    }

    override fun getBasePresenter(): BaseContract.Presenter? {
        return presenter
    }

    private fun setUpToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(true)
    }

    private fun deviceNeedsToBeOnPortraitMode(): Boolean {
        return resources.getBoolean(R.bool.portrait_only)
    }

    private fun forceToPortraitMode() {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    override fun renderMarvelItems(fromIndex: Int, data: List<MarvelItem>?) {
        val adapter = rvData.adapter as HomeDetailAdapter
        adapter.updateData(fromIndex, data)
    }

    override fun onErrorWhenFetching(msg: String?) {
        showSnackbarMessage(msg)
    }

    private fun showSnackbarMessage(msg: String?) {
        Snackbar.make(coordinator_layout, msg!!, Snackbar.LENGTH_LONG)
            .setAction(android.R.string.yes, null).show()
    }

    private fun setUpMarvelCharactersRecyclerView() {
        val spanCount = resources.getInteger(R.integer.items_per_column)
        val layoutManager = GridLayoutManager(this, spanCount)
        rvData.layoutManager = layoutManager
        rvData.setHasFixedSize(false)
        rvData.addItemDecoration(GridItemDecoration(10, spanCount))
        val adapter =
            HomeDetailAdapter { view, item ->
                val iv = view.ivMarvelThumbnail
                onMarvelItemClicked(iv, item)
            }
        val scrollListener = getCustomScrollListener(layoutManager)
        rvData.addOnScrollListener(scrollListener)
        rvData.adapter = adapter
    }

    private fun getCustomScrollListener(layoutManager: LinearLayoutManager): RecyclerView.OnScrollListener {
        return object : EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                fetchMoreData(page)
            }

        }
    }

    private fun setUpSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener {
            fetchMoreData(currentOffset)
        }
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary)
    }

    private fun onMarvelItemClicked(view: View, item: MarvelItem?) {
        showSnackbarMessage("Clicked ${item?.id} - ${item?.name}")
        val options = ActivityOptionsCompat
            .makeSceneTransitionAnimation(this, view, ViewCompat.getTransitionName(view)!!)
        val intent = Intent(this, DetailScreenActivity::class.java)
        intent.putExtra(MarvelItem.TAG, item?.id)
        intent.putExtra(MarvelItem.NAME, item?.name)
        startActivity(intent, options.toBundle())
    }

    private fun fetchInitialMarvelCharacters() {
        currentOffset = 0
        presenter.getMarvelCharacters(currentOffset)
    }

    private fun fetchMoreData(offset: Int?) {
        currentOffset = offset
        presenter.getMarvelCharacters(offset)
    }
}