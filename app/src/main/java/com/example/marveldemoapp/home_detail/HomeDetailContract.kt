package com.example.marveldemoapp.home_detail

import com.example.marveldemoapp.base.BaseContract
import com.example.marveldemoapp.models.MarvelItem

interface HomeDetailContract {

    interface View : BaseContract.View {
        fun renderMarvelItems(fromIndex: Int, data: List<MarvelItem>?)
        fun onErrorWhenFetching(msg: String?)
        fun showLoading()
        fun stopLoading()
    }

    interface Presenter : BaseContract.Presenter {
        fun getMarvelCharacters(offset: Int?)
    }

    interface Interactor : BaseContract.Interactor {
        fun fetchMarvelData(offset: Int?)

        interface ResponseListener {
            fun onMarvelDataFetched(fromIndex: Int, data: List<MarvelItem>?)
            fun onErrorFetchingData(msg: String?)
        }
    }
}