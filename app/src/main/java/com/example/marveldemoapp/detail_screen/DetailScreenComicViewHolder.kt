package com.example.marveldemoapp.detail_screen

import android.view.View
import android.widget.ImageView
import com.example.marveldemoapp.base.decorator.viewHolder.ViewHolderDecorator
import com.example.marveldemoapp.models.Comic
import com.example.marveldemoapp.utils.ImageLoader
import kotlinx.android.synthetic.main.item_comic.view.*

class DetailScreenComicViewHolder(itemView: View) : ViewHolderDecorator<Comic>(itemView),
    ImageLoader.Handler {

    override fun bind(model: Comic?, onClickedItem: (view: View, Comic?) -> Unit) {
        super.bind(model, onClickedItem)
        itemView.txtComicName.text = model?.title
    }

    override fun getImageViewTarget(): ImageView {
        return itemView.ivComicImage
    }
}