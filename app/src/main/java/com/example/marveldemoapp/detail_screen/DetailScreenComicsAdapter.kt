package com.example.marveldemoapp.detail_screen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.marveldemoapp.R
import com.example.marveldemoapp.base.decorator.adapter.AdapterDecorator
import com.example.marveldemoapp.base.decorator.viewHolder.ViewHolderDecorator
import com.example.marveldemoapp.models.Comic

class DetailScreenComicsAdapter(onClickListener: (view: View, Comic?) -> Unit) :
    AdapterDecorator<Comic, ViewHolderDecorator<Comic>>(onClickListener) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderDecorator<Comic> {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_comic, parent, false)
        return DetailScreenComicViewHolder(v)
    }
}