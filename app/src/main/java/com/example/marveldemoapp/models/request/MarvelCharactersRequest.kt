package com.example.marveldemoapp.models.request

import android.content.Context
import com.example.marveldemoapp.R
import com.example.marveldemoapp.base.decorator.request.RequestDecorator
import org.koin.core.KoinComponent
import org.koin.core.inject

class MarvelCharactersRequest : RequestDecorator(), KoinComponent {

    private val limit: Int
        get() {
            return getLimitAccordingToDeviceSpecs()
        }

    private fun getLimitAccordingToDeviceSpecs(): Int {
        val context: Context by inject()
        return context.resources.getInteger(R.integer.max_items_allowed)
    }

    var offset: Int? = null
        set(value) {
            if (value != null) {
                field = value * limit
            }
        }

    override fun getParams(): MutableMap<String, String?> {
        val params = super.getParams()

        params["limit"] = limit.toString()
        params["offset"] = offset.toString()

        return params
    }

}