package com.example.marveldemoapp.models

import com.example.marveldemoapp.base.interfaces.Model
import com.google.gson.annotations.SerializedName

class MarvelItem : Model {

    companion object {
        val TAG: String = MarvelItem::class.java.simpleName
        val NAME:String = "NAME"
    }

    @SerializedName("id")
    var id: String? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("description")
    var description: String? = null
        get() {
            return if (field.equals("")) "No description" else field
        }
    @SerializedName("thumbnail")
    var thumbnail: Thumbnail? = null

    override fun getComicTitle(): String? {
        return name
    }

    override fun getComicThumbnail(): Thumbnail? {
        return thumbnail
    }
}